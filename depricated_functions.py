# Has been replaced with a more efficient function that uses pandas instead
def animate(i):
    ts  = []
    yvs = []
    # yis = []
    visibleData = sftp.open(fileString, 'r').read()
    vLines = visibleData.split(b'\n')
    for line in vLines:
        if len(line) > 1:
            t, yv = line.split(b',')
            t  = int(t)
            yv = int(yv)
            # yi = int(yi)
            ts.append(t)
            yvs.append(yv)
            # yis.append(yi)
    ax1.clear()
    # Moves graph"window" along, always showing the most recent 60 seconds of results.
    try:
        ax1.set_xlim(ts[-60], ts[-1])
    except:
        ax1.set_xlim(0, ts[-1])
    ax1.plot(ts, yvs, color = '#F04854', linestyle = 'solid', label="Visible Light")
    # ax1.plot(ts, yis, color = '#41B0C1', linestyle = 'solid', label="Infrared Light")
    ax1.set_title("Adafruit TSL 2591 Data vs Time")
    ax1.set_xlabel("Time (s)")
    ax1.set_ylabel("Luminosity (lux)")
    ax1.legend(loc="upper left")