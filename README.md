# Pi Zero Adafruit TSL 2591 Plotter

A simple program which plots the visible and infrared readings from an Adafruit TSL 2561 detector over SSH in real time.

## Demonstration
![Live Plotting Demo](demo/demo.gif)

## Installation
Host dependencies:
```bash
pip3 install paramiko matplotlib
```
Pi Zero dependencies:
```bash
pip3 install adafruit-circuitpython-busdevice adafruit-circuitpython-lis3dh adafruit-circuitpython-tsl2591 pigpio Pillow numpy setuptools
```
Copy `photodetector.py` to pi zero, and enable ssh on pi.
Lastly create we need to create a data directory in the pi user's home folder, to store all of the sensor data. This can be done with the following command:
```bash
mkdir /home/pi/data
```

## License
[GNU GPLv3](https://gitlab.com/watermelonvotary/pi-zero-adafruit-tsl-2561-plotter/-/blob/master/LICENSE)
