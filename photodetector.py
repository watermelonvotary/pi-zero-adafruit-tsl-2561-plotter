import os
import sys
import time

import board
import busio
import adafruit_tsl2591
from datetime import date

POLL_RATE = 1  # Second delay before the graph is updated

# Initialize the I2C bus.
i2c = busio.I2C(board.SCL, board.SDA)

# Initialize the sensor.
sensor = adafruit_tsl2591.TSL2591(i2c)

sensor.gain = adafruit_tsl2591.GAIN_LOW
# You can optionally change the gain and integration time:
# sensor.gain = adafruit_tsl2591.GAIN_LOW (1x gain)
# sensor.gain = adafruit_tsl2591.GAIN_MED (25x gain, the default)
# sensor.gain = adafruit_tsl2591.GAIN_MAX (9876x gain)
# sensor.integration_time = adafruit_tsl2591.INTEGRATIONTIME_100MS (100ms, default)
# sensor.integration_time = adafruit_tsl2591.INTEGRATIONTIME_200MS (200ms)
# sensor.integration_time = adafruit_tsl2591.INTEGRATIONTIME_300MS (300ms)
# sensor.integration_time = adafruit_tsl2591.INTEGRATIONTIME_400MS (400ms)
# sensor.integration_time = adafruit_tsl2591.INTEGRATIONTIME_500MS (500ms)
# sensor.integration_time = adafruit_tsl2591.INTEGRATIONTIME_600MS (600ms)

dateString = date.today().strftime("%d-%m-%Y")
fileString = "Photodetector_data_" + dateString + ".txt"
startTime = time.time()


def log_data(timeElapsed, visibleLight):
    dataString = str(timeElapsed) + "," + str(visibleLight) + "\n"
    dataFile = open(fileString, "a")
    dataFile.write(dataString)
    dataFile.close()
    return dataString


while True:
    try:
        for i in range(2):
            timeElapsed = round(startTime - time.time())
            visibleLight = sensor.visible
            printString = log_data(timeElapsed, visibleLight)
        print(printString)
    except KeyboardInterrupt:
        print("Exiting Program...")
        sys.exit()
