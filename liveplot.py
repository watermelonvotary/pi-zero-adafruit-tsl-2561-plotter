import os
import time

import paramiko
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.animation as animation
from matplotlib import style
from datetime import date

__author__ = "Caleb Leinz"
__credits__ = ["Caleb Leinz"]
__license__ = "GPL v3.0"
__version__ = "1.0.0"
__maintainer__ = "Caleb Leinz"
__email__ = "NA"
__status__ = "Dev"

################################################################################

POLL_RATE = 1000  # MS delay before the graph is updated
WINDOW_SIZE = 60  # Max number of seconds show on plot at once
PI_ADDRESS = "169.234.63.99"  # IP address of raspberry pi

style.use("Solarize_Light2")  # Adjusts the style of the plot
fig = plt.figure()  # Create a figure, then axes on which to plot our data
ax1 = plt.axes()

# Create title and axis label information
ax1.set_title("Adafruit TSL 2591 Data vs Time")
ax1.set_xlabel("Time (s)")
ax1.set_ylabel("Luminosity (lux)")

# Create sftp client connection to pull file for graphing
# and run data collection program on pi
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

# Connect to the hostname that the raspberry pi is assigned to, this can be the local
# host if connecting over USB
ssh.connect(hostname=PI_ADDRESS, username="pi", password="spectrovoltbestteam", port=22)
ssh.exec_command("python3 ~/stms-photodetector/data_collector.py")
sftp = ssh.open_sftp()
time.sleep(5)  # sleep to ensure the pi zero has time to start running the client code
dateString = date.today().strftime("%d-%m-%Y")
fileString = "data_" + dateString + ".txt"


def animatepp(i):
    visibleData = sftp.open(fileString, "r")
    df = pd.read_csv(visibleData, sep=",", header=None, names=["Time", "Visible"])
    # Moves graph"window" along, always showing the most recent 60 seconds of results.
    if df.shape[0] >= WINDOW_SIZE:
        ax1.set_xlim(df.iloc[-WINDOW_SIZE, 0], df.iloc[-1, 0])
    else:
        ax1.set_xlim(0, df.iloc[-1, 0])
    ax1.plot(
        df.loc[:, "Time"],
        df.loc[:, "Visible"],
        color="#F04854",
        linestyle="solid",
        label="Visible Light",
    )


ani = animation.FuncAnimation(fig, animatepp, interval=POLL_RATE)
plt.show()

# Cleanup
# AFTER RUNNING CLOSE THE PLOT. DO NOT TERMINATE THE CODE OR ELSE THE PI ZERO WILL
# CONTINUE LOGGING DATA WHICH MAY CAUSE ISSUES
ssh.exec_command("pkill -9 -f data_collector.py")
ssh.close()
sftp.close()
